import { ApproverGuard } from './guards/approver.guard';
import { HeaderService } from './services/header.service';
import { ConstantService } from './services/constant.service';
import { LoadingServiceService } from './services/loading-service.service';
import { UtilityService } from './services/utility.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LeftComponent } from './components/left/left.component';
import { RightComponent } from './components/right/right.component';
import { MainService } from './services/main.service';
import { HeaderComponent } from './components/header/header.component';
import { Routes, RouterModule } from '@angular/router';
import { StatisticsComponent } from './components/statistics/statistics.component';
import { CreateComponent } from './components/create/create.component';
import { ViewComponent } from './components/view/view.component';
import { ApproveComponent } from './components/approve/approve.component';
import { AllComponent } from './components/all/all.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsModule } from 'ng2-charts';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { MessageBoxComponent } from './components/message-box/message-box.component';
import { InformationBoxComponent } from './components/information-box/information-box.component';
import { LoadingComponent } from './components/loading/loading.component';
import { TasksComponent } from './components/tasks/tasks.component';
import { TicketDetailsComponent } from './components/ticket-details/ticket-details.component';
import { MatDialogModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { ConfirmBoxComponent } from './components/confirm-box/confirm-box.component';
import { DragDropDirective } from './directive/drag-drop.directive';
import { DelegateComponent } from './components/delegate/delegate.component';

const routes: Routes = [
  {
    path: '',
    component: StatisticsComponent,
  },
  {
    path: 'statistics',
    component: StatisticsComponent,
  },
  {
    path: 'create',
    component: CreateComponent,
  },
  {
    path: 'edit/:id',
    component: CreateComponent,
  },
  {
    path: 'view',
    component: ViewComponent,
    canActivate: [ApproverGuard]
  },
  {
    path: 'approve',
    component: ApproveComponent,
    canActivate: [ApproverGuard]
  },
  {
    path: 'all',
    component: AllComponent,
  },
  {
    path: 'tasks',
    component: TasksComponent,
    canActivate: [ApproverGuard]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LeftComponent,
    RightComponent,
    HeaderComponent,
    StatisticsComponent,
    CreateComponent,
    ViewComponent,
    ApproveComponent,
    AllComponent,
    PageNotFoundComponent,
    MessageBoxComponent,
    InformationBoxComponent,
    LoadingComponent,
    TasksComponent,
    TicketDetailsComponent,
    ConfirmBoxComponent,
    DragDropDirective,
    DelegateComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes, { useHash: true }),
    ChartsModule,
    BrowserAnimationsModule,
    MatDialogModule,
    HttpClientModule
  ],
  providers: [
    MainService,
    LoadingServiceService,
    UtilityService,
    ConstantService,
    HeaderService
  ],
  entryComponents: [
    TicketDetailsComponent,
    MessageBoxComponent,
    ConfirmBoxComponent,
    DelegateComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
