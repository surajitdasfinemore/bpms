import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConstantService {
  allManagersRoleName = 'bpmsApprover';
  managerRoleName = 'manager';
  financeRoleName = 'finance';
  normalRoleName = 'normal';

  // LOCAL MACHINE CODE
  // appUrl = 'http://sag-tec-dev-03:5555';
  // appUrl = '';
  // appName = 'BPMS-DEMO';
  // SAMLToken =  '';

  // userId = 'bpmsm2';
  // sessionId = '';
  // fullName = 'Surajit Das';
  // email = 'bpmsu1@findmore.pt';

  // userRole = 'bpmsDemo';
  // userRole = 'bpmsDemo|bpmsApprover|manager';
  // userRole = 'bpmsDemo|bpmsApprover|finance';
  // managerList = "bpmsm1|bpmsm2";
  // financeManagerList = "bpmsfm1|bpmsfm2";

  // MWS CODE
  appUrl = window['endpointAddress'];
  appName = window['appName'];
  SAMLToken =  window['SAMLToken'];

  userId = window['userId'];
  sessionId = window['sessionId'];
  fullName = window['fullName'];
  email = window['email'];

  userRole = window['userRoles'];
  managerList = window['managerList'];
  financeManagerList = window['financeManagerList'];

  // All rest addresses are to be configured here
  restUrls = {
    getStatistics: this.appUrl + '/restv2/getOverallStatistics',
    createTicket: this.appUrl + '/restv2/createOrUpdateTicket',
    updateTicket: this.appUrl + '/restv2/createOrUpdateTicket',
    getAllTicket: this.appUrl + '/restv2/getAllTickets',
    updateFav: this.appUrl + '/restv2/updateFavourite',
    getTasks: this.appUrl + '/restv2/getTaskListByUserId',
    getNotification: this.appUrl + '/restv2/getNotifications',
    updateNotification: this.appUrl + '/restv2/updateNotification',
    actions: this.appUrl + '/restv2/actions',
    getCounts: this.appUrl + '/restv2/getCounts'
  };

  // All the technical keys for different statuses are to be configured here
  displayKeys = {
    pendingWithManager: 'pendingWithManager',
    pendingWithFinance: 'pendingWithFinance',
    pendingForCorrection: 'pendingForCorrection',
    rejected: 'rejected',
    completed: 'completed'
  };

  // All the display names for different statues are to be configured here
  displayNames = {
    pendingWithManager: 'Pending With Manager',
    pendingWithFinance: 'Pending With Finance',
    pendingForCorrection: 'Pending For Correction',
    rejected: 'Rejected',
    completed: 'Completed'
  };

  // All rest call messages are to be configured here
  errMessages = {
    somethingWentWrong: 'Something went wrong. Please contact administrator.',
    ticketCreated: 'Expense has been created successfully',
    approveTicket: 'Do you want to APPROVE this request ?',
    returnForCorrectionTicket: 'Do you want to RETURN this request for CORRECTION ?',
    rejectTicket: 'Do you want to REJECT this request ?',
    supportedFiles: 'Only supported file extensions are JPEG, PNG and PDF.'
  };

  // All create/edit expense validations are be to configured here
  validataionMsgs = {
    addName: 'Please fill your name.',
    addEmail: 'Please add email address.',
    addContact: 'Please add contact number.',
    addExpType: 'Please select Expense type.',
    addAmt: 'Please add claim amount',
    uploadDoc: 'Please upload expense bill receipt(s).'
  }

  constructor() { }
}
