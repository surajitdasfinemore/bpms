import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoadingServiceService {
  counter = 0;
  loadingStatus$: BehaviorSubject<any> = new BehaviorSubject('');
  constructor() {
  }

  // Method for showing loading loader
  showLoading() {
    this.counter++;
    this.loadingStatus$.next(true);
  }

  // Method for hiding loading loader
  hideLoading() {
    this.counter--;
    if (this.counter === 0) {
      this.loadingStatus$.next(false);
    }
  }
}
