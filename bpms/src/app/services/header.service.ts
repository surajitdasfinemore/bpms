import { UtilityService } from './utility.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {
  userType = '';
  allManagers = [];
  managerList = [];
  financeManagerList = [];
  ticketData = [];

  constructor(private utilityService: UtilityService) { }

  // Method for getting user type from login user
  getUserType() {
    this.userType;
  }

  // Method for seting user type from login user
  setUserType(type) {
    this.userType = type;
  }

  // Method for setting ticket data
  setTicketData(data) {
    this.ticketData = data;
  }

  // Method for getting all user list of bpmsApprover MWS Role
  getAllManagerList() {
    const allManagers = window['allManagersList'];
    this.allManagers = [];
    if (!this.utilityService.isEmpty(allManagers)) {
      const list = allManagers.split('|');
      for (let index = 0; index < list.length; index++) {
        this.allManagers.push(list[index]);
      }
    }
  }

  // Method for getting all user list of manager MWS Role
  getManagerList() {
    const managerList = window['managerList'];
    this.managerList = [];
    if (!this.utilityService.isEmpty(managerList)) {
      const list = managerList.split('|');
      for (let index = 0; index < list.length; index++) {
        this.managerList.push(list[index]);
      }
    }
  }

  // Method for getting all user list of finance MWS Role
  getFinanceManagerList() {
    const financeManagerList = window['financeList'];
    this.financeManagerList = [];
    if (!this.utilityService.isEmpty(financeManagerList)) {
      const list = financeManagerList.split('|');
      for (let index = 0; index < list.length; index++) {
        this.financeManagerList.push(list[index]);
      }
    }
  }
}
