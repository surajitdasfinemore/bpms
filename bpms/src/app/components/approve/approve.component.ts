import { DelegateComponent } from './../delegate/delegate.component';
import { ConfirmBoxComponent } from './../confirm-box/confirm-box.component';
import { MessageBoxComponent } from './../message-box/message-box.component';
import { HeaderService } from './../../services/header.service';
import { UtilityService } from './../../services/utility.service';
import { Component, OnInit } from '@angular/core';
import { TicketDetailsComponent } from '../ticket-details/ticket-details.component';
import { ConstantService } from 'src/app/services/constant.service';
import { MainService } from 'src/app/services/main.service';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-approve',
  templateUrl: './approve.component.html',
  styleUrls: ['./approve.component.css']
})
export class ApproveComponent implements OnInit {
  dates = [
    {
      date: new Date(),
      incidents: [
        {
          creator: 'Surajit Das',
          amount: 2500,
          requestedDate: '05/08/2019',
          type: 'Hotel'
        },
        {
          creator: 'Surajit Das',
          amount: 500,
          requestedDate: '01/08/2019',
          type: 'Meals'
        },
        {
          creator: 'Surajit Das',
          amount: 5500,
          requestedDate: '01/08/2019',
          type: 'Marketing'
        }
      ]
    }
  ];
  subscriptions = [];
  layout = 'list';
  onlyFav = false;
  displayNames: any;
  ticketData: any = [];
  ticketDataReceived = [];

  userType;
  keySearch = '';

  constructor(private utilityService: UtilityService,
    private constantService: ConstantService,
    private headerService: HeaderService,
    private mainService: MainService,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.userType = this.headerService.userType;
    this.displayNames = this.constantService.displayNames;
    this.onlyFav = this.mainService.onlyFavourite;

    // Getting all data base on the userType
    const subscribe = this.mainService.getAllTickets(this.userType).subscribe(res => {
      if (res.status.statusCode === '1') {
        this.ticketData = res.uiResponse.ticketData;
        if (this.headerService.userType === this.constantService.managerRoleName) {
          this.ticketData = this.ticketData.filter(ticket => ticket.status === this.constantService.displayKeys.pendingWithManager);
        } else if (this.headerService.userType === this.constantService.financeRoleName) {
          this.ticketData = this.ticketData.filter(ticket => ticket.status === this.constantService.displayKeys.pendingWithFinance);
        }
        this.ticketDataReceived = JSON.parse(JSON.stringify(this.ticketData));
        if (this.onlyFav) {
          this.ticketData = this.getOnlyFavouriteTickets();
        }
      } else {
        this.dialog.open(MessageBoxComponent, {
          width: '50vw',
          panelClass: 'message-box-panel',
          data: {
            message: res.status.message
          },
          disableClose: true
        });
      }
    }, err => {
      // TODO: Remove after integration
      this.ticketData = this.mainService.ticketData;
      if (this.headerService.userType === this.constantService.managerRoleName) {
        this.ticketData = this.ticketData.filter(ticket => ticket.status === this.constantService.displayKeys.pendingWithManager);
      } else if (this.headerService.userType === this.constantService.financeRoleName) {
        this.ticketData = this.ticketData.filter(ticket => ticket.status === this.constantService.displayKeys.pendingWithFinance);
      }
      this.ticketDataReceived = JSON.parse(JSON.stringify(this.ticketData));
      if (this.onlyFav) {
        this.ticketData = this.getOnlyFavouriteTickets();
      }

      // TODO: Add after integration
      // this.dialog.open(MessageBoxComponent, {
      //   width: '50vw',
      //   panelClass: 'message-box-panel',
      //   data: {
      //     message: this.constantService.errMessages.somethingWentWrong
      //   },
      //   disableClose: true
      // });
    });
    this.subscriptions.push(subscribe);
  }

  // Method for getting data on dd-MMM-yyyy format
  getDateformat(date) {
    return this.utilityService.getDateFormat(new Date(date)).split(' ')[0];
  }

  // Method for toggling favourite expense for an user
  updateFavourite(ticket) {
    const isFav = ticket.isFavourite === 'true' ? 'false' : 'true';
    const sub = this.mainService.updateFavriate(ticket.id, isFav).subscribe(res => {
      if (res.status.statusCode === '1') {
        ticket.isFavourite = res.uiResponse.isFavourite;
        for (let index = 0; index < this.ticketDataReceived.length; index++) {
          if (ticket.id === this.ticketDataReceived[index].id) {
            this.ticketDataReceived[index].isFavourite = ticket.isFavourite;
            break;
          }
        }
        if (isFav === 'true') {
          this.mainService.increaseFav$.next(1);
        } else if (isFav === 'false') {
          this.mainService.decreaseFav$.next(1);
        }
      } else {
        this.dialog.open(MessageBoxComponent, {
          width: '50vw',
          panelClass: 'message-box-panel',
          data: {
            message: res.status.message
          },
          disableClose: true
        });
      }
    }, err => {
      this.dialog.open(MessageBoxComponent, {
        width: '50vw',
        panelClass: 'message-box-panel',
        data: {
          message: this.constantService.errMessages.somethingWentWrong
        },
        disableClose: true
      });
    });
    this.subscriptions.push(sub);
  }

  // Method for filtering only favourite expense ticket
  getOnlyFavouriteTickets() {
    return this.ticketData.filter(data => data.isFavourite === 'true');
  }

  // Method for getting expense ticket data based on the favourite flag is enabled or not
  updateTicketData() {
    this.onlyFav = !this.onlyFav;
    this.mainService.onlyFavourite = this.onlyFav;
    if (this.onlyFav) {
      this.ticketData = this.getOnlyFavouriteTickets();
    } else {
      this.ticketData = JSON.parse(JSON.stringify(this.ticketDataReceived));
    }
  }

  // Method for showing the expense data details for their actions
  showMore(ticket) {
    const diaRef = this.dialog.open(TicketDetailsComponent, {
      width: '50vw',
      panelClass: 'ticket-details-panel',
      data: {
        data: ticket
      },
      disableClose: true
    });

    const sub = diaRef.afterClosed().subscribe(data => {
      if (data.action === 'approve') {
        this.approveTicket(data.data);
      } else if (data.action === 'correction') {
        this.returnForCorrectionTicket(data.data);
      } else if (data.action === 'reject') {
        this.rejectTicket(data.data);
      } else if (data.action === 'delegate') {
        this.delegateTicket(data.data);
      }
    });

    this.subscriptions.push(sub);
  }

  // Method for opening pop up for manager's delegation action
  delegateTicket(ticket) {
    const dialogRef = this.dialog.open(DelegateComponent, {
      width: '600px',
      panelClass: 'delegate-box-panel',
      disableClose: true
    });

    const subs = dialogRef.afterClosed().subscribe((response) => {
      if (this.utilityService.isDefined(response) && response.status === true) {
        const sub = this.mainService.actions(ticket.id, 'delegate', response.userId).subscribe(res => {
          if (res.status.statusCode === '1') {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
          } else {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
          }
        }, err => {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.somethingWentWrong
            },
            disableClose: true
          });
        });
        this.subscriptions.push(sub);
      }
    });
    this.subscriptions.push(subs);
  }

  // Method for approving expense ticket by manager or finance manager
  approveTicket(ticket) {
    const dialogRef = this.dialog.open(ConfirmBoxComponent, {
      width: '600px',
      panelClass: 'confirm-box-panel',
      data: {
        message: this.constantService.errMessages.approveTicket
      },
      disableClose: true
    });

    const subs = dialogRef.afterClosed().subscribe((response) => {
      if (this.utilityService.isDefined(response) && response === true) {
        if (ticket.status === this.constantService.displayKeys.pendingWithManager) {
          ticket.status = this.constantService.displayKeys.pendingWithFinance;
        } else {
          ticket.status = this.constantService.displayKeys.completed;
        }

        const sub = this.mainService.actions(ticket.id, 'approve', '').subscribe(res => {
          if (res.status.statusCode === '1') {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
            this.updateReceiveCopyStatus(ticket.id, res.uiResponse.status);
          } else {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
          }
        }, err => {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.somethingWentWrong
            },
            disableClose: true
          });
        });
        this.subscriptions.push(sub);
      }
    });
    this.subscriptions.push(subs);
  }

  // Method for correcting expense ticket by manager or finance manager
  returnForCorrectionTicket(ticket) {
    const dialogRef = this.dialog.open(ConfirmBoxComponent, {
      width: '600px',
      panelClass: 'confirm-box-panel',
      data: {
        message: this.constantService.errMessages.returnForCorrectionTicket
      },
      disableClose: true
    });

    const subs = dialogRef.afterClosed().subscribe((response) => {
      if (this.utilityService.isDefined(response) && response === true) {
        ticket.status = this.constantService.displayKeys.pendingForCorrection;
        const sub = this.mainService.actions(ticket.id, 'correction', '').subscribe(res => {
          if (res.status.statusCode === '1') {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
            this.updateReceiveCopyStatus(ticket.id, res.uiResponse.status);
          } else {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
          }
        }, err => {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.somethingWentWrong
            },
            disableClose: true
          });
        });
        this.subscriptions.push(sub);
      }
    });
    this.subscriptions.push(subs);
  }

  // Method for rejecting expense ticket by manager or finance manager
  rejectTicket(ticket) {
    ticket.status = this.constantService.displayKeys.rejected;
    const dialogRef = this.dialog.open(ConfirmBoxComponent, {
      width: '600px',
      panelClass: 'confirm-box-panel',
      data: {
        message: this.constantService.errMessages.rejectTicket
      },
      disableClose: true
    });

    const subs = dialogRef.afterClosed().subscribe((response) => {
      if (this.utilityService.isDefined(response) && response === true) {
        ticket.status = this.constantService.displayKeys.rejected;
        const sub = this.mainService.actions(ticket.id, 'reject', '').subscribe(res => {
          if (res.status.statusCode === '1') {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
            this.updateReceiveCopyStatus(ticket.id, res.uiResponse.status);
          } else {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
          }
        }, err => {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.somethingWentWrong
            },
            disableClose: true
          });
        });
        this.subscriptions.push(sub);
      }
    });
    this.subscriptions.push(subs);
  }

  // Method for updating the status of passed expense ticket to expense data received copy
  updateReceiveCopyStatus(id, status) {
    for (let index = 0; index < this.ticketDataReceived.length; index++) {
      if (this.ticketDataReceived[index].id === id) {
        this.ticketDataReceived[index].status = status;
        break;
      }
    }
  }

  // Method for adding class based on the user search on the search box in approve page
  saerchResult(ticket) {
    if (ticket.createdBy.name.toLowerCase().indexOf(this.keySearch.toLowerCase()) !== -1
        || ticket.status.toLowerCase().indexOf(this.keySearch.toLowerCase()) !== -1
        || ticket.type.toLowerCase().indexOf(this.keySearch.toLowerCase()) !== -1
        || ticket.amount.toString().toLowerCase().indexOf(this.keySearch.toLowerCase()) !== -1) {
      return 'show-ticket';
    } else {
      return 'hide-ticket';
    }
  }

  // Method for release memory on destroy component
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}
