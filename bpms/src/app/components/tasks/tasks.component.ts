import { DelegateComponent } from './../delegate/delegate.component';
import { ConfirmBoxComponent } from './../confirm-box/confirm-box.component';
import { MessageBoxComponent } from './../message-box/message-box.component';
import { TicketDetailsComponent } from './../ticket-details/ticket-details.component';
import { ConstantService } from 'src/app/services/constant.service';
import { HeaderService } from './../../services/header.service';
import { MainService } from 'src/app/services/main.service';
import { UtilityService } from './../../services/utility.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit, OnDestroy {
  subscriptions = [];
  tasks = [];
  tasksReceived = [];
  showFile = false;
  imgUrl = '';
  pdfUrl:any = '';
  days:any = 'all';
  constructor(private utilityService: UtilityService,
    private headerService: HeaderService,
    private constantService: ConstantService,
    private dialog: MatDialog,
    private sanitizer: DomSanitizer,
    private mainService: MainService) { }

  ngOnInit() {
    this.getTaskData();
  }

  // Method for making active the filter based on the user click and get data besed on the user clicked days and its user type
  setDaysAndGetTaskData(days) {
    this.days = days;
    this.getTaskData();
  }

  // Method for getting the task data by passing its user type and days has been selected (bydefault = 'all')
  getTaskData() {
    const sub = this.mainService.getTasks(this.headerService.userType, this.days).subscribe(res => {
      if (res.status.statusCode === '1') {
        this.tasks = res.uiResponse.taskList;
        this.tasksReceived = JSON.parse(JSON.stringify(this.tasks));
      } else {
        this.dialog.open(MessageBoxComponent, {
          width: '50vw',
          panelClass: 'message-box-panel',
          data: {
            message: res.status.message
          },
          disableClose: true
        });
      }
    }, err => {
      // TODO: Remove later
      this.tasks = this.mainService.ticketData;
      if (this.headerService.userType === this.constantService.managerRoleName) {
        this.tasks = this.tasks.filter(task => task.status === this.constantService.displayKeys.pendingWithManager);
      } else if (this.headerService.userType === this.constantService.financeRoleName) {
        this.tasks = this.tasks.filter(task => task.status === this.constantService.displayKeys.pendingWithFinance);
      }
      this.tasksReceived = JSON.parse(JSON.stringify(this.tasks));

      // TODO: need to uncomment this code
      // this.dialog.open(MessageBoxComponent, {
      //   width: '50vw',
      //   panelClass: 'message-box-panel',
      //   data: {
      //     message: this.constantService.errMessages.somethingWentWrong
      //   },
      //   disableClose: true
      // });
    });
    this.subscriptions.push(sub);
  }

  // Method for closing the task pop up box and go back to its back page from it has been opened
  goBack() {
    this.utilityService.back();
  }

  // Method for approving an expense ticket based on the user clicked on the expense
  openTicketForApproval(ticket, index) {
    const diaRef = this.dialog.open(TicketDetailsComponent, {
      width: '50vw',
      panelClass: 'ticket-details-panel',
      data: {
        data: ticket
      },
      disableClose: true
    });

    const sub = diaRef.afterClosed().subscribe(data => {
      if (data.action === 'approve') {
        this.approveTicket(data.data, index);
      } else if (data.action === 'correction') {
        this.returnForCorrectionTicket(data.data, index);
      } else if (data.action === 'reject') {
        this.rejectTicket(data.data, index);
      } else if (data.action === 'delegate') {
        this.delegateTicket(data.data);
      }
    });

    this.subscriptions.push(sub);
  }

  // Method for opening a popup which will allow a manager to delegate his task to his manager
  delegateTicket(ticket) {
    const dialogRef = this.dialog.open(DelegateComponent, {
      width: '600px',
      panelClass: 'delegate-box-panel',
      disableClose: true
    });

    const subs = dialogRef.afterClosed().subscribe((response) => {
      if (this.utilityService.isDefined(response) && response.status === true) {
        const sub = this.mainService.actions(ticket.id, 'delegate', response.userId).subscribe(res => {
          if (res.status.statusCode === '1') {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
          } else {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
          }
        }, err => {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.somethingWentWrong
            },
            disableClose: true
          });
        });
        this.subscriptions.push(sub);
      }
    });
    this.subscriptions.push(subs);
  }

  // Method for approving an expense ticket based on the user clicked
  approveTicket(ticket, index) {
    const dialogRef = this.dialog.open(ConfirmBoxComponent, {
      width: '600px',
      panelClass: 'confirm-box-panel',
      data: {
        message: this.constantService.errMessages.approveTicket
      },
      disableClose: true
    });

    const subs = dialogRef.afterClosed().subscribe((response) => {
      if (this.utilityService.isDefined(response) && response === true) {
        if (ticket.status === this.constantService.displayKeys.pendingWithManager) {
          ticket.status = this.constantService.displayKeys.pendingWithFinance;
        } else {
          ticket.status = this.constantService.displayKeys.completed;
        }

        const sub = this.mainService.actions(ticket.id, 'approve', '').subscribe(res => {
          if (res.status.statusCode === '1') {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
            this.tasks.splice(index, 1);
            this.tasksReceived.splice(index, 1);
          } else {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
          }
        }, err => {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.somethingWentWrong
            },
            disableClose: true
          });
        });
        this.subscriptions.push(sub);
      }
    });
    this.subscriptions.push(subs);
  }

  // Method for correcting an expense ticket based on the user clicked
  returnForCorrectionTicket(ticket, index) {
    const dialogRef = this.dialog.open(ConfirmBoxComponent, {
      width: '600px',
      panelClass: 'confirm-box-panel',
      data: {
        message: this.constantService.errMessages.returnForCorrectionTicket
      },
      disableClose: true
    });

    const subs = dialogRef.afterClosed().subscribe((response) => {
      if (this.utilityService.isDefined(response) && response === true) {
        ticket.status = this.constantService.displayKeys.pendingForCorrection;
        const sub = this.mainService.actions(ticket.id, 'correction', '').subscribe(res => {
          if (res.status.statusCode === '1') {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
            this.tasks.splice(index, 1);
            this.tasksReceived.splice(index, 1);
          } else {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
          }
        }, err => {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.somethingWentWrong
            },
            disableClose: true
          });
        });
        this.subscriptions.push(sub);
      }
    });
    this.subscriptions.push(subs);
  }

  // Method for rejecting an expense ticket based on the user clicked
  rejectTicket(ticket, index) {
    const dialogRef = this.dialog.open(ConfirmBoxComponent, {
      width: '600px',
      panelClass: 'confirm-box-panel',
      data: {
        message: this.constantService.errMessages.rejectTicket
      },
      disableClose: true
    });

    const subs = dialogRef.afterClosed().subscribe((response) => {
      if (this.utilityService.isDefined(response) && response === true) {
        ticket.status = this.constantService.displayKeys.rejected;
        const sub = this.mainService.actions(ticket.id, 'reject', '').subscribe(res => {
          if (res.status.statusCode === '1') {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
            this.tasks.splice(index, 1);
            this.tasksReceived.splice(index, 1);
          } else {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
          }
        }, err => {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.somethingWentWrong
            },
            disableClose: true
          });
        });
        this.subscriptions.push(sub);
      }
    });
    this.subscriptions.push(subs);
  }

  // Method to show the file on a pop box based on its file extension type
  showFilePopup(doc) {
    this.showFile = true;
    if (doc.extension === 'image/jpeg' || doc.extension === 'image/jpg' || doc.extension === 'image/png') {
      this.imgUrl = doc.base64;
    } else if (doc.extension === 'application/pdf') {
      this.pdfUrl = this.sanitizer.bypassSecurityTrustResourceUrl(doc.base64);
    }
  }

  // Method for closing the file show pop up box
  closeFile() {
    this.showFile = false;
    this.imgUrl = '';
    this.pdfUrl = '';
  }

  // Method for releasing memory of the component while destroying
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}
