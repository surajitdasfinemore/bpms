import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MainService } from 'src/app/services/main.service';

@Component({
  selector: 'app-right',
  templateUrl: './right.component.html',
  styleUrls: ['./right.component.css']
})
export class RightComponent implements OnInit, OnDestroy {
  subscriptions = [];
  collapse;
  notificationTab = 'all';
  notifications = [];
  notificationReceived = [];

  constructor(private mainService: MainService,
    private router: Router) { }

  ngOnInit() {
    this.mainService.collapse$.subscribe(status => this.collapse = status);
    this.notificationReceived = JSON.parse(JSON.stringify(this.notifications));
  }

  // Method for toggling the left panel to its left on user click on the hide button
  collapsibleLeftPanel() {
    this.mainService.collapse$.next(!this.collapse);
  }

  // Method for opening tasks pop up box component for the logged in manager or finance manager
  gotoTask() {
    this.router.navigate(['/', 'tasks']);
  }

  // Method for filtering the notifications only unread notifications
  getUnreadNotificationCount() {
    return this.notificationReceived.filter(data => data.isRead === 'false').length;
  }

  // Method for updating notification data based on the filter user clicked
  updateNatificationTab(name) {
    this.notificationTab = name;
    if (name === 'all') {
      this.notifications = JSON.parse(JSON.stringify(this.notificationReceived));
    } else {
      this.notifications = this.notificationReceived.filter(data => data.isRead === name);
    }
  }

  // Method for making a notification read status to true
  makeAsRead(noti) {
    noti.isRead = 'true';
    for (let index = 0; index < this.notificationReceived.length; index++) {
      if (noti.id === this.notificationReceived[index].id) {
        this.notificationReceived[index].isRead = 'true';
        break;
      }
    }
  }

  // Method for toggling the notification drop down box based on the user click
  toggleNotification(event) {
    if (!event.target.classList.contains('head')) return;
    event.currentTarget.classList.toggle('open');
  }

  // Method for releasing the component memory on destroy
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
