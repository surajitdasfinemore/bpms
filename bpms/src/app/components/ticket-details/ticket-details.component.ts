import { HeaderService } from './../../services/header.service';
import { ConstantService } from './../../services/constant.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject, OnDestroy, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-ticket-details',
  templateUrl: './ticket-details.component.html',
  styleUrls: ['./ticket-details.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TicketDetailsComponent implements OnInit, OnDestroy {
  subscriptions = [];
  ticket;
  displayNames:any = {};
  userType;
  showImage = false;
  imgSrc = '';
  pdfSrc:any = '';

  constructor(public dialogRef: MatDialogRef<TicketDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public service: any,
    private sanitizer: DomSanitizer,
    private constantService: ConstantService,
    private headerService: HeaderService) { }

  ngOnInit() {
    this.userType = this.headerService.userType;
    this.displayNames = this.constantService.displayNames;
    this.ticket = this.service.data;
  }

  // Method for closing the pop up box with reference data passed to its parent
  closeDialog(action) {
    this.dialogRef.close({
      action: action,
      data: this.ticket
    });
  }

  // Method for showing files for viewing the uploading document
  showFile(doc) {
    if (doc.extension === 'image/jpeg' || doc.extension === 'image/jpg' || doc.extension === 'image/png') {
      this.showImage = true;
      this.imgSrc = doc.base64;
    } else if (doc.extension === 'application/pdf') {
      this.showImage = true;
      this.pdfSrc = this.sanitizer.bypassSecurityTrustResourceUrl(doc.base64);
    }
    document.getElementById('ticketdata').scrollTop = 0;
  }

  // Method for releasing memory on this component destroy
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
