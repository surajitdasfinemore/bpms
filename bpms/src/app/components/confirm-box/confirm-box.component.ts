import { Component, OnInit, OnDestroy, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-confirm-box',
  templateUrl: './confirm-box.component.html',
  styleUrls: ['./confirm-box.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ConfirmBoxComponent implements OnInit, OnDestroy {
  subscriptions = [];
  message = '';
  constructor(public dialogRef: MatDialogRef<ConfirmBoxComponent>,
    @Inject(MAT_DIALOG_DATA) public service: any) { }

  ngOnInit() {
    this.message = this.service.message;
  }

  // Method for closing dialog based on the user action
  closeDialog(status) {
    this.dialogRef.close(status);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
