import { HeaderService } from './../../services/header.service';
import { MainService } from 'src/app/services/main.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  subscriptions = [];
  activeTab = '';
  userType;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private mainService: MainService,
    private headerService: HeaderService) { }

  ngOnInit() {
    this.userType = this.headerService.userType;
    const subs = this.mainService.tabChange$.subscribe((name) => {
      if (name === '') {
        name = 'statistics';
      }
      this.activeTab = name;
    });
    this.subscriptions.push(subs);

    const sub = this.route.params.subscribe(tab => {
      if (window.location.href.indexOf('create') !== -1 || window.location.href.indexOf('edit') !== -1) {
        this.activeTab = 'create';
      } else if (window.location.href.indexOf('view') !== -1) {
        this.activeTab = 'view';
      } else if (window.location.href.indexOf('approve') !== -1) {
        this.activeTab = 'approve';
      } else if (window.location.href.indexOf('all') !== -1) {
        this.activeTab = 'all';
      } else {
        this.activeTab = 'statistics';
      }
    });
    this.subscriptions.push(sub);
  }

  // Method for updating the tab based on the user click on the tabs
  changeActiveTab(name) {
    this.activeTab = name;
    this.router.navigate(['/', name]);
    if (name === 'all') {
      this.mainService.allPageFilters$.next('all');
    }
  }

  // Method for releasing the memory on component destory
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}
