import { MessageBoxComponent } from './../message-box/message-box.component';
import { MainService } from 'src/app/services/main.service';
import { ConstantService } from 'src/app/services/constant.service';
import { UtilityService } from './../../services/utility.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  subscriptions = [];
  ticketType = 'Create';
  ticketId;

  createTicket:any = {
    id: '',
    createdBy: {
      empId: '',
      name: '',
      email: '',
      contact: ''
    },
    createdOn: '',
    amount: '',
    evidenceDocs: [],
    noOfDocs: 0,
    status: '',
    lastModificationDate: '',
    type: '',
    comment: ''
  };

  createTicketReceived;
  validations:any = [];

  constructor(private utilityService: UtilityService,
    private constantService: ConstantService,
    private dialog: MatDialog,
    private router: Router,
    private mainService: MainService,
    private activatedRouter: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRouter.paramMap.subscribe(data => {
      if (this.utilityService.isDefined(data['params']) && this.utilityService.isDefined(data['params'].id)) {
        this.ticketType = 'Edit';
        this.ticketId = data['params'].id;
        this.createTicket = this.mainService.editTicketData;
      }
    });

    this.createTicket.createdBy.empId = this.constantService.userId;
    this.createTicket.createdBy.name = this.constantService.fullName;
    this.createTicket.createdBy.email = this.constantService.email;

    this.createTicketReceived = JSON.parse(JSON.stringify(this.createTicket));
  }

  // Method for sending request for create or edit an expense ticket details after checking validations
  createOrUpdateTicket() {
    this.validations = [];
    if (this.utilityService.isEmpty(this.createTicket.createdBy.name)) {
      this.validations.push({
        msg: this.constantService.validataionMsgs.addName
      });
    }

    if (this.utilityService.isEmpty(this.createTicket.createdBy.email)) {
      this.validations.push({
        msg: this.constantService.validataionMsgs.addEmail
      });
    }

    if (this.utilityService.isEmpty(this.createTicket.createdBy.contact)) {
      this.validations.push({
        msg: this.constantService.validataionMsgs.addContact
      });
    }

    if (this.utilityService.isEmpty(this.createTicket.type)) {
      this.validations.push({
        msg: this.constantService.validataionMsgs.addExpType
      });
    }

    if (this.utilityService.isEmpty(this.createTicket.amount.toString())) {
      this.validations.push({
        msg: this.constantService.validataionMsgs.addAmt
      });
    }

    if (this.createTicket.evidenceDocs.length === 0) {
      this.validations.push({
        msg: this.constantService.validataionMsgs.uploadDoc
      });
    }

    if (this.validations.length === 0) {
      this.createTicket.status = this.constantService.displayKeys.pendingWithManager;
      this.createTicket.noOfDocs = this.createTicket.evidenceDocs.length;
      this.createTicket.createdOn = this.utilityService.getDateFormat(new Date()).split(' ')[0];
      this.createTicket.lastModificationDate = this.utilityService.getDateFormat(new Date()).split(' ')[0];
      const sub = this.mainService.createOrUpdateTicket(this.createTicket).subscribe(res => {
        if (res.status.statusCode === '1') {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: res.status.message
            },
            disableClose: true
          });

          if (this.createTicket.id !== '') {
            this.mainService.updateMyTickets$.next(1);
          }

          this.createTicket = {
            id: '',
            createdBy: {
              empId: this.constantService.userId,
              name: this.constantService.fullName,
              email: this.constantService.email,
              contact: ''
            },
            createdOn: '',
            amount: '',
            evidenceDocs: [],
            status: '',
            lastModificationDate: '',
            type: '',
            comment: ''
          };
          this.router.navigate(['/', 'statistics']);
        } else {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: res.status.message
            },
            disableClose: true
          });
        }
      }, err => {
        this.dialog.open(MessageBoxComponent, {
          width: '50vw',
          panelClass: 'message-box-panel',
          data: {
            message: this.constantService.errMessages.somethingWentWrong
          },
          disableClose: true
        });

        // TODO: REMOVE THIS CODE
        this.createTicket = {
          id: '',
          createdBy: {
            empId: this.constantService.userId,
            name: this.constantService.fullName,
            email: this.constantService.email,
            contact: ''
          },
          createdOn: new Date().toString(),
          amount: '',
          evidenceDocs: [],
          status: '',
          lastModificationDate: new Date().toString(),
          type: '',
          comment: ''
        };
        this.router.navigate(['/', 'create']);
      });
      this.subscriptions.push(sub);
    }
  }

  // Method for clearing the data when user clicks on the clear ticket data
  clearTicket() {
    this.createTicket = JSON.parse(JSON.stringify(this.createTicketReceived));
  }

  // Method for closing validation messages on click on close mark
  closeValidationTicket(index) {
    this.validations.splice(index, 1);
  }

  // Method for uploading files using input file html
  uploadFiles(event) {
    for (let index = 0; index < event.target.files.length; index++) {
      if (!(event.target.files[index].type === 'application/pdf'
          || event.target.files[index].type === 'image/jpg'
          || event.target.files[index].type === 'image/jpeg'
          || event.target.files[index].type === 'image/png')) {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.supportedFiles
            },
            disableClose: true
          });
          return;
      }
    }

    this.createTicket.evidenceDocs = [];
    for(let index = 0; index < event.target.files.length; index++) {
      this.createTicket.evidenceDocs.push({
        fileId: '',
        ticketId: '',
        filename: event.target.files[index].name,
        extension: event.target.files[index].type,
        size: event.target.files[index].size,
        base64: this.toBase64(event.target.files[index], index)
      });
    }
  }

  // Method for clicking upload file internally
  clickUploadFile() {
    document.getElementById('fileUploadInput').click();
  }

  // Method for removing uploading files
  removeFile(index) {
    this.createTicket.evidenceDocs.splice(index, 1);
  }

  // Method for converting file stream to base64
  toBase64(file, index) {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.createTicket.evidenceDocs[index].base64 = reader.result;
    }
  }

  // Method for reading file drops in file drop zone
  fileDrop(files) {
    for (let index = 0; index < files.length; index++) {
      if (!(files[index].type === 'application/pdf'
          || files[index].type === 'image/jpg'
          || files[index].type === 'image/jpeg'
          || files[index].type === 'image/png')) {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.supportedFiles
            },
            disableClose: true
          });
          return;
      }
    }
    for (let index = 0; index < files.length; index++) {
      this.createTicket.evidenceDocs.push({
        fileId: '',
        ticketId: '',
        filename: files[index].name,
        extension: files[index].type,
        size: files[index].size,
        base64: this.toBase64(files[index], this.createTicket.evidenceDocs.length)
      });
    }
  }

  // Method for releasing memory while destroying component
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
