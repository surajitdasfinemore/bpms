import { ConfirmBoxComponent } from './../confirm-box/confirm-box.component';
import { UtilityService } from './../../services/utility.service';
import { Router } from '@angular/router';
import { MessageBoxComponent } from './../message-box/message-box.component';
import { HeaderService } from './../../services/header.service';
import { MainService } from 'src/app/services/main.service';
import { ConstantService } from './../../services/constant.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.css']
})
export class AllComponent implements OnInit, OnDestroy {
  subscriptions = [];
  displayKeys:any = {};
  displayNames:any = {};
  statusFilters = {
    pendingWithManager: true,
    pendingWithFinance: true,
    pendingForCorrection: true,
    rejected: true,
    completed: true
  };

  ticketData: any = [];
  ticketDataReceived: any = [];

  loggedInUserId;
  userType;
  keySearch = '';
  onlyFav = false;
  showFilters = true;
  requestType;

  constructor(private constantService: ConstantService,
    private mainService: MainService,
    private dialog: MatDialog,
    private router: Router,
    private utilityService: UtilityService,
    private headerService: HeaderService) { }

  ngOnInit() {
    this.loggedInUserId = this.constantService.userId;
    this.userType = this.headerService.userType;
    this.displayKeys = this.constantService.displayKeys;
    this.displayNames = this.constantService.displayNames;
    this.onlyFav = this.mainService.onlyFavourite;
    this.requestType = this.mainService.requestType || 'all';

    // Getting all data base on the userType
    const subscribe = this.mainService.getAllTickets(this.userType).subscribe(res => {
      if (res.status.statusCode === '1') {
        this.ticketData = res.uiResponse.ticketData;
        this.ticketDataReceived = JSON.parse(JSON.stringify(this.ticketData));
        this.updateData();
      } else {
        this.dialog.open(MessageBoxComponent, {
          width: '50vw',
          panelClass: 'message-box-panel',
          data: {
            message: res.status.message
          },
          disableClose: true
        });
      }
    }, err => {
      // TODO: Remove after integration
      this.ticketData = this.mainService.ticketData;
      this.ticketDataReceived = JSON.parse(JSON.stringify(this.ticketData));
      this.updateData();

      // TODO: Add after integration
      // this.dialog.open(MessageBoxComponent, {
      //   width: '50vw',
      //   panelClass: 'message-box-panel',
      //   data: {
      //     message: this.constantService.errMessages.somethingWentWrong
      //   },
      //   disableClose: true
      // });
    });
    this.subscriptions.push(subscribe);

    const sub = this.mainService.allPageFilters$.subscribe(status => {
      if (status === 'all') {
        this.statusFilters = {
          pendingWithManager: true,
          pendingWithFinance: true,
          pendingForCorrection: true,
          rejected: true,
          completed: true
        };
      } else if (status === 'correction') {
        this.statusFilters = {
          pendingWithManager: false,
          pendingWithFinance: false,
          pendingForCorrection: true,
          rejected: false,
          completed: false
        };
      } else if (status === 'rejected') {
        this.statusFilters = {
          pendingWithManager: false,
          pendingWithFinance: false,
          pendingForCorrection: false,
          rejected: true,
          completed: false
        };
      }
      this.updateData();
    });
    this.subscriptions.push(sub);
  }

  // Method for updating the expense data based on the statuses which are selected by the user/automatically
  updateData() {
    this.ticketData = [];
    for (let index = 0; index < this.ticketDataReceived.length; index++) {
      if (this.statusFilters.pendingWithManager && this.ticketDataReceived[index].status === this.displayKeys.pendingWithManager) {
        this.ticketData.push(this.ticketDataReceived[index]);
      }
      if (this.statusFilters.pendingWithFinance && this.ticketDataReceived[index].status === this.displayKeys.pendingWithFinance) {
        this.ticketData.push(this.ticketDataReceived[index]);
      }
      if (this.statusFilters.pendingForCorrection && this.ticketDataReceived[index].status === this.displayKeys.pendingForCorrection) {
        this.ticketData.push(this.ticketDataReceived[index]);
      }
      if (this.statusFilters.rejected && this.ticketDataReceived[index].status === this.displayKeys.rejected) {
        this.ticketData.push(this.ticketDataReceived[index]);
      }
      if (this.statusFilters.completed && this.ticketDataReceived[index].status === this.displayKeys.completed) {
        this.ticketData.push(this.ticketDataReceived[index]);
      }
    }

    // If favourite clicked anywhere within the project then it will filter the data based on the isFavourite flag
    if (this.onlyFav) {
      this.ticketData = this.getOnlyFavouriteTickets();
    }

    // If request type clicked for my request only then filtering the expense data further
    if (this.mainService.requestType === 'my') {
      this.ticketData = this.getReqeustTypeData();
    }
  }

  // Method for updating expense data based on the user click on the filters
  updateStatusFilters(event, status) {
    this.statusFilters[status] = event.target.checked;
    this.updateData();
  }

  // Method for getting the status count by passing status
  getDataByStatus(status) {
    return this.ticketDataReceived.filter(data => data.status === status).length;
  }

  // Method for getting filter data based on the directly filter clicked by the user
  filter(status) {
    this.statusFilters = {
      pendingWithManager: false,
      pendingWithFinance: false,
      pendingForCorrection: false,
      rejected: false,
      completed: false
    }

    this.statusFilters[status] = true;
    this.updateData();
  }

  // Method for putting comman in Amount value of the expense
  toLocal(data) {
    if (isNaN(data)) return data;
    try {
      return parseInt(data).toLocaleString();
    } catch(e) {
      return data;
    }
  }

  // Method for updating a ticket to user's favourite list
  updateFavourite(ticket) {
    const isFav = ticket.isFavourite === 'true' ? 'false' : 'true';
    const sub = this.mainService.updateFavriate(ticket.id, isFav).subscribe(res => {
      if (res.status.statusCode === '1') {
        ticket.isFavourite = res.uiResponse.isFavourite;
        for (let index = 0; index < this.ticketDataReceived.length; index++) {
          if (ticket.id === this.ticketDataReceived[index].id) {
            this.ticketDataReceived[index].isFavourite = ticket.isFavourite;
            break;
          }
        }
        if (isFav === 'true') {
          this.mainService.increaseFav$.next(1);
        } else if (isFav === 'false') {
          this.mainService.decreaseFav$.next(1);
        }
      } else {
        this.dialog.open(MessageBoxComponent, {
          width: '50vw',
          panelClass: 'message-box-panel',
          data: {
            message: res.status.message
          },
          disableClose: true
        });
      }
    }, err => {
      this.dialog.open(MessageBoxComponent, {
        width: '50vw',
        panelClass: 'message-box-panel',
        data: {
          message: this.constantService.errMessages.somethingWentWrong
        },
        disableClose: true
      });
    });
    this.subscriptions.push(sub);
  }

  // Method for approving user expense passed by the expense ticket
  approveTicket(ticket) {
    const dialogRef = this.dialog.open(ConfirmBoxComponent, {
      width: '600px',
      panelClass: 'confirm-box-panel',
      data: {
        message: this.constantService.errMessages.approveTicket
      },
      disableClose: true
    });

    const subs = dialogRef.afterClosed().subscribe((response) => {
      if (this.utilityService.isDefined(response) && response === true) {
        if (ticket.status === this.constantService.displayKeys.pendingWithManager) {
          ticket.status = this.constantService.displayKeys.pendingWithFinance;
        } else {
          ticket.status = this.constantService.displayKeys.completed;
        }

        const sub = this.mainService.actions(ticket.id, 'approve', '').subscribe(res => {
          if (res.status.statusCode === '1') {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
            this.updateReceiveCopyStatus(ticket.id, res.uiResponse.status);
          } else {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
          }
        }, err => {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.somethingWentWrong
            },
            disableClose: true
          });
        });
        this.subscriptions.push(sub);
      }
    });
    this.subscriptions.push(subs);
  }

  // Method for correction user expense passed by the expense ticket
  returnForCorrectionTicket(ticket) {
    const dialogRef = this.dialog.open(ConfirmBoxComponent, {
      width: '600px',
      panelClass: 'confirm-box-panel',
      data: {
        message: this.constantService.errMessages.returnForCorrectionTicket
      },
      disableClose: true
    });

    const subs = dialogRef.afterClosed().subscribe((response) => {
      if (this.utilityService.isDefined(response) && response === true) {
        ticket.status = this.constantService.displayKeys.pendingForCorrection;
        const sub = this.mainService.actions(ticket.id, 'correction', '').subscribe(res => {
          if (res.status.statusCode === '1') {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
            this.updateReceiveCopyStatus(ticket.id, res.uiResponse.status);
          } else {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
          }
        }, err => {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.somethingWentWrong
            },
            disableClose: true
          });
        });
        this.subscriptions.push(sub);
      }
    });
    this.subscriptions.push(subs);
  }

  // Method for rejecting user expense passed by the expense ticket
  rejectTicket(ticket) {
    ticket.status = this.constantService.displayKeys.rejected;
    const dialogRef = this.dialog.open(ConfirmBoxComponent, {
      width: '600px',
      panelClass: 'confirm-box-panel',
      data: {
        message: this.constantService.errMessages.rejectTicket
      },
      disableClose: true
    });

    const subs = dialogRef.afterClosed().subscribe((response) => {
      if (this.utilityService.isDefined(response) && response === true) {
        ticket.status = this.constantService.displayKeys.rejected;
        const sub = this.mainService.actions(ticket.id, 'reject', '').subscribe(res => {
          if (res.status.statusCode === '1') {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
            this.updateReceiveCopyStatus(ticket.id, res.uiResponse.status);
          } else {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
          }
        }, err => {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.somethingWentWrong
            },
            disableClose: true
          });
        });
        this.subscriptions.push(sub);
      }
    });
    this.subscriptions.push(subs);
  }

  // Method for updating status of this received copy
  updateReceiveCopyStatus(id, status) {
    for (let index = 0; index < this.ticketDataReceived.length; index++) {
      if (this.ticketDataReceived[index].id === id) {
        this.ticketDataReceived[index].status = status;
        break;
      }
    }
  }

  // Method for navigating the user to edit expense page
  editTicket(ticket) {
    this.mainService.editTicketData = ticket;
    this.router.navigate(['/', 'edit', ticket.id]);
  }

  // Method which returns only favourite expense data
  getOnlyFavouriteTickets() {
    return this.ticketData.filter(data => data.isFavourite === 'true');
  }

  // Method for filtering expense data based on the favourite is enabled or not
  updateTicketData() {
    this.onlyFav = !this.onlyFav;
    this.mainService.onlyFavourite = this.onlyFav;
    if (this.onlyFav) {
      this.ticketData = this.getOnlyFavouriteTickets();
    } else {
      this.ticketData = JSON.parse(JSON.stringify(this.ticketDataReceived));
    }
  }

  // Method for putting class based on the match found in the expense data and user type search text
  saerchResult(ticket) {
    if (ticket.createdBy.name.toLowerCase().indexOf(this.keySearch.toLowerCase()) !== -1
        || ticket.status.toLowerCase().indexOf(this.keySearch.toLowerCase()) !== -1
        || ticket.type.toLowerCase().indexOf(this.keySearch.toLowerCase()) !== -1
        || ticket.amount.toString().toLowerCase().indexOf(this.keySearch.toLowerCase()) !== -1) {
      return 'show-ticket';
    } else {
      return 'hide-ticket';
    }
  }

  // Method for changing the expense ticket types
  changeType(type) {
    this.requestType = type;
    this.mainService.requestType = type;
    this.updateData();
  }

  // Method for filtering expense based on type
  getReqeustTypeData() {
    return this.ticketData.filter(data => data.createdBy.empId.toLowerCase() === this.constantService.userId.toLowerCase());
  }

  // Method for releasing memory on destroy
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}
