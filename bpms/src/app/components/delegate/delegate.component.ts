import { ConstantService } from 'src/app/services/constant.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject, OnDestroy, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-delegate',
  templateUrl: './delegate.component.html',
  styleUrls: ['./delegate.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DelegateComponent implements OnInit, OnDestroy {
  subscriptions = [];
  userId = '';
  userList = [];
  showError = false;

  constructor(public dialogRef: MatDialogRef<DelegateComponent>,
    private constantService: ConstantService,
    @Inject(MAT_DIALOG_DATA) public service: any) { }

  ngOnInit() {
    this.userList = this.constantService.managerList.split('|').filter(user => user !== this.constantService.userId);
  }

  // Method for closing the dialog box by passing user action to its parent component
  closeDialog(status) {
    if (status) {
      if (this.userId === '') {
        this.showError = true;
        return;
      }
    }
    this.dialogRef.close({
      status: status,
      userId: this.userId
    });
  }

  // Method for hiding error message when user changes his selection
  updateUserId() {
    this.showError = false;
  }

  // Method for releasing memery on conponent destory
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
