import { ConfirmBoxComponent } from './../confirm-box/confirm-box.component';
import { HeaderService } from './../../services/header.service';
import { MessageBoxComponent } from './../message-box/message-box.component';
import { MatDialog } from '@angular/material';
import { MainService } from 'src/app/services/main.service';
import { ConstantService } from './../../services/constant.service';
import { UtilityService } from './../../services/utility.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { TicketDetailsComponent } from '../ticket-details/ticket-details.component';
import { DelegateComponent } from '../delegate/delegate.component';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit, OnDestroy {

  subscriptions = [];
  userType;
  layout = 'list';
  onlyFav = false;
  displayNames: any;
  ticketData: any = [];
  ticketDataReceived = [];
  keySearch = '';

  constructor(private utilityService: UtilityService,
    private constantService: ConstantService,
    private headerService: HeaderService,
    private mainService: MainService,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.displayNames = this.constantService.displayNames;
    this.userType = this.headerService.userType;
    this.onlyFav = this.mainService.onlyFavourite;

    // Getting all data base on the userType
    const subscribe = this.mainService.getAllTickets(this.userType).subscribe(res => {
      if (res.status.statusCode === '1') {
        this.ticketData = res.uiResponse.ticketData;
        if (this.headerService.userType === this.constantService.managerRoleName) {
          this.ticketData = this.ticketData.filter(ticket => ticket.status === this.constantService.displayKeys.pendingWithManager);
        } else if (this.headerService.userType === this.constantService.financeRoleName) {
          this.ticketData = this.ticketData.filter(ticket => ticket.status === this.constantService.displayKeys.pendingWithFinance);
        }
        this.ticketDataReceived = JSON.parse(JSON.stringify(this.ticketData));
        if (this.onlyFav) {
          this.ticketData = this.getOnlyFavouriteTickets();
        }
      } else {
        this.dialog.open(MessageBoxComponent, {
          width: '50vw',
          panelClass: 'message-box-panel',
          data: {
            message: res.status.message
          },
          disableClose: true
        });
      }
    }, err => {
      // TODO: Remove after integration
      this.ticketData = this.mainService.ticketData;
      if (this.headerService.userType === this.constantService.managerRoleName) {
        this.ticketData = this.ticketData.filter(ticket => ticket.status === this.constantService.displayKeys.pendingWithManager);
      } else if (this.headerService.userType === this.constantService.financeRoleName) {
        this.ticketData = this.ticketData.filter(ticket => ticket.status === this.constantService.displayKeys.pendingWithFinance);
      }
      this.ticketDataReceived = JSON.parse(JSON.stringify(this.ticketData));
      if (this.onlyFav) {
        this.ticketData = this.getOnlyFavouriteTickets();
      }
      // TODO: Add after integration
      // this.dialog.open(MessageBoxComponent, {
      //   width: '50vw',
      //   panelClass: 'message-box-panel',
      //   data: {
      //     message: this.constantService.errMessages.somethingWentWrong
      //   },
      //   disableClose: true
      // });
    });
    this.subscriptions.push(subscribe);
  }

  // Method for getting a date on dd-MMM-yyyy format
  getDateformat(date) {
    return this.utilityService.getDateFormat(new Date(date)).split(' ')[0];
  }

  // Method for updating a favourite list by the user on clicking on the star link of an expense ticket
  updateFavourite(ticket) {
    const isFav = ticket.isFavourite === 'true' ? 'false' : 'true';
    const sub = this.mainService.updateFavriate(ticket.id, isFav).subscribe(res => {
      if (res.status.statusCode === '1') {
        ticket.isFavourite = res.uiResponse.isFavourite;
        for (let index = 0; index < this.ticketDataReceived.length; index++) {
          if (ticket.id === this.ticketDataReceived[index].id) {
            this.ticketDataReceived[index].isFavourite = ticket.isFavourite;
            break;
          }
        }
        if (isFav === 'true') {
          this.mainService.increaseFav$.next(1);
        } else if (isFav === 'false') {
          this.mainService.decreaseFav$.next(1);
        }
      } else {
        this.dialog.open(MessageBoxComponent, {
          width: '50vw',
          panelClass: 'message-box-panel',
          data: {
            message: res.status.message
          },
          disableClose: true
        });
      }
    }, err => {
      this.dialog.open(MessageBoxComponent, {
        width: '50vw',
        panelClass: 'message-box-panel',
        data: {
          message: this.constantService.errMessages.somethingWentWrong
        },
        disableClose: true
      });
    });
    this.subscriptions.push(sub);
  }

  // Method for getting tickets which are marked favourite by the logged in user
  getOnlyFavouriteTickets() {
    return this.ticketData.filter(data => data.isFavourite === 'true');
  }

  // Method for getting expense data based on the favourite flag set by the user
  updateTicketData() {
    this.onlyFav = !this.onlyFav;
    this.mainService.onlyFavourite = this.onlyFav;
    if (this.onlyFav) {
      this.ticketData = this.getOnlyFavouriteTickets();
    } else {
      this.ticketData = JSON.parse(JSON.stringify(this.ticketDataReceived));
    }
  }

  // Method for opening a popup for showing an expense ticket details and take some actions by the user
  showMore(ticket) {
    const diaRef = this.dialog.open(TicketDetailsComponent, {
      width: '50vw',
      panelClass: 'ticket-details-panel',
      data: {
        data: ticket
      },
      disableClose: true
    });

    const sub = diaRef.afterClosed().subscribe(data => {
      if (data.action === 'approve') {
        this.approveTicket(data.data);
      } else if (data.action === 'correction') {
        this.returnForCorrectionTicket(data.data);
      } else if (data.action === 'reject') {
        this.rejectTicket(data.data);
      } else if (data.action === 'delegate') {
        this.delegateTicket(data.data);
      }
    });

    this.subscriptions.push(sub);
  }

  // Method for opening an delegate pop up component where a manager can delegate tickets
  delegateTicket(ticket) {
    const dialogRef = this.dialog.open(DelegateComponent, {
      width: '600px',
      panelClass: 'delegate-box-panel',
      disableClose: true
    });

    const subs = dialogRef.afterClosed().subscribe((response) => {
      if (this.utilityService.isDefined(response) && response.status === true) {
        const sub = this.mainService.actions(ticket.id, 'delegate', response.userId).subscribe(res => {
          if (res.status.statusCode === '1') {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
          } else {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
          }
        }, err => {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.somethingWentWrong
            },
            disableClose: true
          });
        });
        this.subscriptions.push(sub);
      }
    });
    this.subscriptions.push(subs);
  }

  // Method for approving a expense ticket based on the user clicked action
  approveTicket(ticket) {
    const dialogRef = this.dialog.open(ConfirmBoxComponent, {
      width: '600px',
      panelClass: 'confirm-box-panel',
      data: {
        message: this.constantService.errMessages.approveTicket
      },
      disableClose: true
    });

    const subs = dialogRef.afterClosed().subscribe((response) => {
      if (this.utilityService.isDefined(response) && response === true) {
        if (ticket.status === this.constantService.displayKeys.pendingWithManager) {
          ticket.status = this.constantService.displayKeys.pendingWithFinance;
        } else {
          ticket.status = this.constantService.displayKeys.completed;
        }

        const sub = this.mainService.actions(ticket.id, 'approve', '').subscribe(res => {
          if (res.status.statusCode === '1') {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
            this.updateReceiveCopyStatus(ticket.id, res.uiResponse.status);
          } else {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
          }
        }, err => {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.somethingWentWrong
            },
            disableClose: true
          });
        });
        this.subscriptions.push(sub);
      }
    });
    this.subscriptions.push(subs);
  }

  // Method for correcting a expense ticket based on the user clicked action
  returnForCorrectionTicket(ticket) {
    const dialogRef = this.dialog.open(ConfirmBoxComponent, {
      width: '600px',
      panelClass: 'confirm-box-panel',
      data: {
        message: this.constantService.errMessages.returnForCorrectionTicket
      },
      disableClose: true
    });

    const subs = dialogRef.afterClosed().subscribe((response) => {
      if (this.utilityService.isDefined(response) && response === true) {
        ticket.status = this.constantService.displayKeys.pendingForCorrection;
        const sub = this.mainService.actions(ticket.id, 'correction', '').subscribe(res => {
          if (res.status.statusCode === '1') {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
            this.updateReceiveCopyStatus(ticket.id, res.uiResponse.status);
          } else {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
          }
        }, err => {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.somethingWentWrong
            },
            disableClose: true
          });
        });
        this.subscriptions.push(sub);
      }
    });
    this.subscriptions.push(subs);
  }

  // Method for rejecting a expense ticket based on the user clicked action
  rejectTicket(ticket) {
    const dialogRef = this.dialog.open(ConfirmBoxComponent, {
      width: '600px',
      panelClass: 'confirm-box-panel',
      data: {
        message: this.constantService.errMessages.rejectTicket
      },
      disableClose: true
    });

    const subs = dialogRef.afterClosed().subscribe((response) => {
      if (this.utilityService.isDefined(response) && response === true) {
        ticket.status = this.constantService.displayKeys.rejected;
        const sub = this.mainService.actions(ticket.id, 'reject', '').subscribe(res => {
          if (res.status.statusCode === '1') {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
            this.updateReceiveCopyStatus(ticket.id, res.uiResponse.status);
          } else {
            this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
          }
        }, err => {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.somethingWentWrong
            },
            disableClose: true
          });
        });
        this.subscriptions.push(sub);
      }
    });
    this.subscriptions.push(subs);
  }

  // Method for updating the status of expense ticket based on the ticket id has been passed to its received copy
  updateReceiveCopyStatus(id, status) {
    for (let index = 0; index < this.ticketDataReceived.length; index++) {
      if (this.ticketDataReceived[index].id === id) {
        this.ticketDataReceived[index].status = status;
        break;
      }
    }
  }

  // Method for adding class based on the user search on the search box
  saerchResult(ticket) {
    if (ticket.createdBy.name.toLowerCase().indexOf(this.keySearch.toLowerCase()) !== -1
        || ticket.status.toLowerCase().indexOf(this.keySearch.toLowerCase()) !== -1
        || ticket.type.toLowerCase().indexOf(this.keySearch.toLowerCase()) !== -1
        || ticket.amount.toString().toLowerCase().indexOf(this.keySearch.toLowerCase()) !== -1) {
      return 'show-ticket';
    } else {
      return 'hide-ticket';
    }
  }

  // Method for releasing memory of this component on destroy
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}
