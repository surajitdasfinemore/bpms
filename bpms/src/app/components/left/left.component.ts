import { MessageBoxComponent } from './../message-box/message-box.component';
import { ConstantService } from 'src/app/services/constant.service';
import { HeaderService } from './../../services/header.service';
import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MainService } from 'src/app/services/main.service';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-left',
  templateUrl: './left.component.html',
  styleUrls: ['./left.component.css']
})
export class LeftComponent implements OnInit, OnDestroy {
  subscriptions = [];
  collapse;
  fullName;
  role;
  userType;
  favCount:any = 0;
  myRequestsCount:any = 0;
  myTasksCount:any = 0;

  constructor(private mainService: MainService,
    private constantService: ConstantService,
    private dialog: MatDialog,
    private headerService: HeaderService,
    private router: Router) { }

  ngOnInit() {
    this.userType = this.headerService.userType;
    this.mainService.collapse$.subscribe(status => this.collapse = status);
    this.fullName = this.constantService.fullName;
    if (this.headerService.userType === this.constantService.managerRoleName) {
      this.role = "Direct Manager";
    } else if (this.headerService.userType === this.constantService.financeRoleName) {
      this.role = "Finance Manager";
    } else {
      this.role = 'Associate Consultant';
    }

    const sub = this.mainService.getCounts(this.headerService.userType).subscribe(res => {
      if (res.status.statusCode === '1') {
        this.favCount = res.uiResponse.favCount;
        this.myRequestsCount = res.uiResponse.myRequestsCount;
        this.myTasksCount = res.uiResponse.myTasksCount;
      } else {
        this.dialog.open(MessageBoxComponent, {
          width: '50vw',
          panelClass: 'message-box-panel',
          data: {
            message: res.status.message
          },
          disableClose: true
        });
      }
    }, err => {
      this.dialog.open(MessageBoxComponent, {
        width: '50vw',
        panelClass: 'message-box-panel',
        data: {
          message: this.constantService.errMessages.somethingWentWrong
        },
        disableClose: true
      });
    });
    this.subscriptions.push(sub);

    const subs = this.mainService.updateMyTickets$.subscribe(status => {
      if (status === 1) {
        this.myRequestsCount = (parseInt(this.myRequestsCount) + 1).toString();
      }
    });
    this.subscriptions.push(subs);

    const subs1 = this.mainService.increaseFav$.subscribe(status => {
      if (status === 1) {
        this.favCount = (parseInt(this.favCount) + 1).toString();
      }
    });
    this.subscriptions.push(subs1);

    const subs2 = this.mainService.decreaseFav$.subscribe(status => {
      if (status === 1) {
        this.favCount = this.favCount - 1;
      }
    });
    this.subscriptions.push(subs2);
  }

  // Method for taking the user to statistics page
  gotoHome() {
    this.router.navigate(['/', 'statistics']);
    this.mainService.tabChange$.next('statistics');
  }

  // Method for opening the notification pop up box
  gotoNotification() {
    this.router.navigate(['/', 'notifications']);
  }

  // Method for opening task pop up box component
  gotoTask() {
    this.router.navigate(['/', 'tasks']);
  }

  // Method for making only favourite flag true and navigate to view or all page based on the user type
  onlyFavouriots() {
    this.mainService.onlyFavourite = true;
    if (this.userType === 'normal') {
      this.router.navigate(['/', 'all']);
      this.mainService.tabChange$.next('all');
    } else {
      this.router.navigate(['/', 'view']);
      this.mainService.tabChange$.next('view');
    }
  }

  // Method for navigating user to all tab
  gotoAllTasks() {
    this.router.navigate(['/', 'all']);
    this.mainService.tabChange$.next('all');
  }

  // Method for navigating user to may task tab
  gotoViewMyTasks() {
    this.mainService.requestType = 'my';
    this.gotoAllTasks();
  }

  // Method for releasing the memory of the component destroy
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
