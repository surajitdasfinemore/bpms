import { ConstantService } from 'src/app/services/constant.service';
import { MessageBoxComponent } from './../message-box/message-box.component';
import { HeaderService } from './../../services/header.service';
import { MainService } from 'src/app/services/main.service';
import { Router } from '@angular/router';
import { UtilityService } from './../../services/utility.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit, OnDestroy {
  subscriptions = [];
  percentage = {
    firstPt: 0,
    secondPt: 0,
    thirdPt: 0
  };

  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [
        {
          ticks: {
            callback: function(label:any, index, labels) {
              let lac:any = (label/1000)+' K';
                return lac;
            }
          },
          scaleLabel: {
              display: true,
              labelString: '1K = 1,000'
          }
        }
      ]
    }
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [
    {
      data: [],
      label: 'Applied',
      backgroundColor: '#17a2b8'
    },
    {
      data: [],
      label: 'Approved',
      backgroundColor: '#28a745'
    },
    {
      data: [],
      label: 'Rejected',
      backgroundColor: '#dc3545'
    }
  ];

  days:any = 7;
  totalTickets:any = 0;
  managerApproveTickets:any = 0;
  financeApproveTickets:any = 0;
  rejectedTickets:any = 0;
  favrioutTickets:any = 0;
  tasks:any = 0;
  notificationCounts:any = 0;
  ticketsPerDay = [];

  userType;

  constructor(private utilService: UtilityService,
    private router: Router,
    private mainService: MainService,
    private headerService: HeaderService,
    private dialog: MatDialog,
    private constantService: ConstantService) { }

  ngOnInit() {
    this.updateAllData();
  }

  // Method for updating all data based on ther user logged in and its user type
  updateAllData() {
    this.userType = this.headerService.userType;
    const sub = this.mainService.getStatistics(this.headerService.userType, this.days).subscribe(res => {
      if (res.status.statusCode === '1') {
        this.totalTickets = res.uiResponse.totalTickets;
        this.managerApproveTickets = res.uiResponse.managerApproveTickets;
        this.financeApproveTickets = res.uiResponse.financeApproveTickets;
        this.rejectedTickets = res.uiResponse.rejectedTickets;
        this.ticketsPerDay = res.uiResponse.ticketsPerDay;

        this.percentage.firstPt = Math.round(this.managerApproveTickets / this.totalTickets * 100);
        this.percentage.secondPt = Math.round(this.financeApproveTickets / this.totalTickets * 100);
        this.percentage.thirdPt = Math.round(this.rejectedTickets / this.totalTickets * 100);
        this.updateAllPercentage();
        this.updateChart();
      } else {
        this.dialog.open(MessageBoxComponent, {
          width: '50vw',
          panelClass: 'message-box-panel',
          data: {
            message: res.status.message
          },
          disableClose: true
        });
      }
    }, err => {
      /* TODO: REMOVE FROM HERE*/
      const res = {
        uiResponse: {
          totalTickets: "13",
          managerApproveTickets: "5",
          financeApproveTickets: "4",
          rejectedTickets: "1",
          ticketsPerDay: [
            {
              date: '20-aug-2020',
              total: '2',
              approved: '1',
              rejected: '1'
            },
            {
              date: '16-aug-2020',
              total: '1',
              approved: '1',
              rejected: '0'
            }
          ],
          favrioutTickets: '2',
          tasks: '10',
          notificationCounts: '15'
        }
      };
      this.totalTickets = res.uiResponse.totalTickets;
      this.managerApproveTickets = res.uiResponse.managerApproveTickets;
      this.financeApproveTickets = res.uiResponse.financeApproveTickets;
      this.rejectedTickets = res.uiResponse.rejectedTickets;
      this.ticketsPerDay = res.uiResponse.ticketsPerDay;

      this.percentage.firstPt = Math.round(this.managerApproveTickets / this.totalTickets * 100);
      this.percentage.secondPt = Math.round(this.financeApproveTickets / this.totalTickets * 100);
      this.percentage.thirdPt = Math.round(this.rejectedTickets / this.totalTickets * 100);
      this.updateAllPercentage();
      this.updateChart();
      /* TODO: REMOVE TILL HERE*/

      /* TODO: Uncomment */
      // this.dialog.open(MessageBoxComponent, {
      //   width: '450px',
      //   panelClass: 'message-box-panel',
      //   data: {
      //     message: this.constantService.errMessages.somethingWentWrong
      //   },
      //   disableClose: true
      // });
    });
    this.subscriptions.push(sub);
  }

  // Method for updating the percentage value based on the passed parameter name and its value
  updatePercentage(name, value) {
    document.getElementById(name).style.setProperty('--percent', value);
  }

  // Method for updating the percentage with a delay in UI
  updateAllPercentage() {
    setTimeout(() => {
      this.updatePercentage('firstPt', this.percentage.firstPt);
      this.updatePercentage('secondPt', this.percentage.secondPt);
      this.updatePercentage('thirdPt', this.percentage.thirdPt);
    }, 100);
  }

  // Method for removing all the graphics and do a rest call for creating the graphs again
  updateDays(days) {
    this.days = days;
    this.barChartData[0].data = [];
    this.barChartData[1].data = [];
    this.barChartData[2].data = [];
    this.barChartLabels = [];
    this.updateAllData();
  }

  // Method for updating chart data
  updateChart() {
    for (let index = 0; index < this.ticketsPerDay.length; index++) {
      this.barChartLabels.push(this.getDateFormatForChart(this.ticketsPerDay[index].date));
      this.barChartData[0].data.push(+this.ticketsPerDay[index].total);
      this.barChartData[1].data.push(+this.ticketsPerDay[index].approved);
      this.barChartData[2].data.push(+this.ticketsPerDay[index].rejected);
    }
  }

  // Method for changing data format to date/month
  getDateFormatForChart(date) {
    try {
      date = new Date(date);
      if (date instanceof Date) {
        const month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'];
        return date.getDate() + '/' + month[date.getMonth()];
      } else {
        return date;
      }
    } catch(e) {
      return date;
    }
  }

  // Method for getting data without precision
  getPrecision(value) {
    return this.utilService.getPrecision(value, 0);
  }

  // Method for navigating user to create ticket page
  gotoCreateTicket() {
    this.router.navigate(['/', 'create']);
    this.mainService.tabChange$.next('create');
  }

  // Method for navigating user to view tickets page
  gotoViewTicket() {
    const route = this.userType === 'normal' ? 'all' : 'view';
    this.router.navigate(['/', route]);
    this.mainService.tabChange$.next(route);
  }

  // Method for navigating user to all ticket page
  gotoAllTicket(type) {
    this.router.navigate(['/', 'all']);
    this.mainService.tabChange$.next('all');
    if (type === 'rejected') {
      this.mainService.allPageFilters$.next('rejected');
    } else if (type === 'correction') {
      this.mainService.allPageFilters$.next('correction');
    } else {
      this.mainService.allPageFilters$.next('all');
    }
  }

  // Method for releasing memory while destroying component
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
