import { MatDialogRef, MatDialog } from '@angular/material';
import { ConstantService } from 'src/app/services/constant.service';
import { HeaderService } from './services/header.service';
import { MessageBoxComponent } from './components/message-box/message-box.component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MainService } from './services/main.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy{
  subscriptions = [];
  userType = '';
  collapse;

  constructor(private mainService: MainService,
    private headerService: HeaderService,
    private constantService: ConstantService,
    private dialog: MatDialog) { }

  ngOnInit() {
    // TODO: SAML Request for creating session with Integration Server
    // this.http.post(this.constantService.restUrls.SAMLUrl,
    //     'SAMLResponse=' + window['SAMLToken'], {
    //     headers: new HttpHeaders({
    //       'Content-Type' : 'x-content/samlassertion',
    //       'sag-authentication-not-required': 'sag-authentication-not-required'
    //     })
    // }).subscribe(res => {
    //   console.log("SAML Successfully done", res);
    // }, err => {
    //   console.error("SAML Failed", err);
    // });

    // Setting up user list based on different roles
    this.headerService.getAllManagerList();
    this.headerService.getManagerList();
    this.headerService.getFinanceManagerList();

    // Setting up userType based on the logged in user role
    this.updateUserType();

    // Method for collapsing the left panel
    const subs = this.mainService.collapse$.subscribe(status => this.collapse = status);
    this.subscriptions.push(subs);
  }

  // Method for updating the user type based on the logged user roles
  updateUserType() {
    if (this.constantService.userRole.indexOf(this.constantService.managerRoleName) !== -1) {
      this.userType = this.constantService.managerRoleName;
    } else if (this.constantService.userRole.indexOf(this.constantService.financeRoleName) !== -1) {
      this.userType = this.constantService.financeRoleName;
    } else {
      this.userType = this.constantService.normalRoleName;
    }
    this.headerService.setUserType(this.userType);
  }

  // Method for releasing memory of this component on destroy
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
