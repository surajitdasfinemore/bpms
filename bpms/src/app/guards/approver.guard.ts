import { ConstantService } from 'src/app/services/constant.service';
import { HeaderService } from './../services/header.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApproverGuard implements CanActivate {
  constructor(private headerService: HeaderService,
    private constantService: ConstantService) {

  }

  // Method for putting guard by which some links are not allowed to redirect by the normal user in the application
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (this.headerService.userType === this.constantService.managerRoleName || this.headerService.userType === this.constantService.financeRoleName) {
        return true;
      }
      return false;
  }
}
